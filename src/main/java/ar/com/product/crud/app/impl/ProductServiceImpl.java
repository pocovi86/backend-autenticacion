package ar.com.product.crud.app.impl;

import ar.com.product.crud.app.entity.Product;
import ar.com.product.crud.app.repository.ProductRepository;
import ar.com.product.crud.app.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductServiceImpl  implements ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    ProductRepository repository;

    @Override
    public List<Product> list() {
        log.debug("Solicitud para obtener la lista de products");
        return repository.findAll();
    }

    @Override
    public Optional<Product> getOne(int id) {
        log.debug("Solicitud para obtener un product por el id: {}", id);
        return repository.findById(id);
    }

    @Override
    public Optional<Product> getByName(String name) {
        log.debug("Solicitud para obtener un product por nombre: {}", name);
        return repository.findByName(name);
    }

    @Override
    public boolean existById(int id) {
        log.debug("Solicitud para verificar un product con el id: {}", id);
        return repository.existsById(id);
    }

    @Override
    public boolean existByName(String name) {
        log.debug("Solicitud para verificar un product por el nombre: {}", name);
        return repository.existsByName(name);
    }

    @Override
    public void save(Product product) {
        log.debug("Solicitud para crear un product: {}", product);
        repository.save(product);
    }

    @Override
    public void delete(int id) {
        log.debug("Solicitud para eliminar un product por el id: {}, ", id);
        repository.deleteById(id);
    }
}
