package ar.com.product.crud.app.security.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginUser {
    @NotBlank
    private String nameUser;
    @NotBlank
    private String password;
}
