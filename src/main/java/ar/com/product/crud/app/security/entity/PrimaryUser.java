package ar.com.product.crud.app.security.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class PrimaryUser implements UserDetails{
    private String name;
    private String userName;
    private String email;
    private String password;
    // el signo de pregunto significa generico y GranteAutority es una clase especifica de la seguridad
    private Collection<? extends GrantedAuthority> authorities;

    public PrimaryUser(String name, String userName, String email, String password, Collection<? extends GrantedAuthority> authorities) {
        this.name = name;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }

    // metodo build: asigna los privilegios a cada usuario es decir su autorizacion si es admin o un usuario comun
    public static PrimaryUser build(User user){

        /* se obtiene una lista de Grandte de Autthority a partir de los roles */
        List<GrantedAuthority> authorities = user.getRols().stream().map(rol -> new SimpleGrantedAuthority(rol.getRolName().name())).collect(Collectors.toList());

        /* devolvemos una clase PrimaryUser */
        return new PrimaryUser(user.getName(), user.getNameUser(), user.getEmail(), user.getPassword(), authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
