package ar.com.product.crud.app.security.jwt;

import ar.com.product.crud.app.security.impl.UserDetailServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *  esta clase se va a ejecutar por cada peticion
 *  va a coprobar que sea valido el token
 *  si es valido el token va a permitir acceso al recurso
 */
public class JwtTokenFilter extends OncePerRequestFilter {

    private final static Logger log = LoggerFactory.getLogger(JwtTokenFilter.class);

    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    UserDetailServiceImpl userDetailService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String token = getToken(request);

            /* si el token no es null y es valido (lo validamos con la clase JwtProvider validateToken(token)) */
            if (token != null && jwtProvider.validateToken(token)) {
                String nameUser = jwtProvider.getUserNameFromToken(token); /* obtenemos el nombre del token */
                UserDetails userDetails = userDetailService.loadUserByUsername(nameUser); /* cargamos el usuario con el nombre obtenido del token */
                UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities()); /* revisamos la autenticacion quien es y que autorizacion tiene */
                SecurityContextHolder.getContext().setAuthentication(auth); /* y se lo pasamos al contexto de la autenticacion */
            }
        } catch (Exception e) {
            log.error("Fail en el metodo doFilter" + e.getMessage());
        }
        filterChain.doFilter(request, response);
    }

    // este metodo es para extraer el token, eliminar la parte builder mas el espacio
    private String getToken(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (header != null && header.startsWith("Bearer")) {
            return header.replace("Bearer ", "");
        }
        return null;
    }
}
