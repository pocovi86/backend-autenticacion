package ar.com.product.crud.app.controller;

import ar.com.product.crud.app.dto.Message;
import ar.com.product.crud.app.dto.ProductDTO;
import ar.com.product.crud.app.entity.Product;
import ar.com.product.crud.app.service.ProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {
    @Autowired
    ProductService service;

    @GetMapping("/listProduct")
    public ResponseEntity<List<Product>> list() {
        List<Product> list = service.list();
        return new ResponseEntity<List<Product>>(list, HttpStatus.OK);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Product> getById(@PathVariable("id") int id) {
        if (!service.existById(id)) {
            return new ResponseEntity(new Message("it doesn't exist"), HttpStatus.NOT_FOUND);
        }
        Product product = service.getOne(id).get();
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @GetMapping("/detailName/{name}")
    public ResponseEntity<Product> getByName(@PathVariable("name") String name) {
        if (!service.existByName(name)) {
            return new ResponseEntity(new Message("it doesn't exist"), HttpStatus.NOT_FOUND);
        }
        Product product = service.getByName(name).get();
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody ProductDTO dto) {
        if (StringUtils.isBlank(dto.getName())) {
            return new ResponseEntity(new Message("The name is compulsory"), HttpStatus.BAD_REQUEST);
        }
        if (dto.getPrice() == null || dto.getPrice() < 0) {
            return new ResponseEntity(new Message("The price should be greater than 0"), HttpStatus.BAD_REQUEST);
        }
        if (service.existByName(dto.getName())) {
            return new ResponseEntity(new Message("This name already exists"), HttpStatus.BAD_REQUEST);
        }
        Product product = new Product(dto.getName(), dto.getPrice());
        service.save(product);
        return new ResponseEntity(new Message("Product created"), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody ProductDTO dto) {
        if (!service.existById(id)) {
            return new ResponseEntity(new Message("it doesn't exist"), HttpStatus.NOT_FOUND);
        }
        if (service.existByName(dto.getName()) && service.getByName(dto.getName()).get().getId() != id) {
            return new ResponseEntity(new Message("This name already exists"), HttpStatus.BAD_REQUEST);
        }
        if (dto.getPrice() < 0) {
            return new ResponseEntity(new Message("The price should be greater than 0"), HttpStatus.BAD_REQUEST);
        }

        Product product = service.getOne(id).get();
        product.setName(dto.getName());
        product.setPrice(dto.getPrice());
        service.save(product);
        return new ResponseEntity(new Message("Product updated"), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") int id){
        if (!service.existById(id)) {
            return new ResponseEntity(new Message("it doesn't exist"), HttpStatus.NOT_FOUND);
        }
        service.delete(id);
        return new ResponseEntity(new Message("Product deleted"), HttpStatus.OK);
    }
}
