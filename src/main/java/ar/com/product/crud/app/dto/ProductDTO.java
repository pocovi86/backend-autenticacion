package ar.com.product.crud.app.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
public class ProductDTO {
    @NotBlank
    private String name;
    @Min(0)
    private Float price;

    public ProductDTO(@NotBlank String name, @Min(0) Float price) {
        this.name = name;
        this.price = price;
    }
}
