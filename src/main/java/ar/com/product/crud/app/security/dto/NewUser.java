package ar.com.product.crud.app.security.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Data
public class NewUser {
    @NotBlank  /* con esto digo que no puede ser una cadena vacia */
    private String name;
    @NotBlank
    private String nameUser;
    @Email
    private String email;
    @NotBlank
    private String password;
    private Set<String> roles = new HashSet<>();
}
