package ar.com.product.crud.app.security.service;

import ar.com.product.crud.app.security.entity.Rol;
import ar.com.product.crud.app.security.enums.RolName;

import java.util.Optional;

public interface RolService {

    /**
     * obtiene un Rol por medio del nombre
     *
     * @param rolName
     * @return {@link Optional} of {@link Rol}
     */
    Optional<Rol> getByRolName(RolName rolName);

    /**
     * guarda un rol en la bd
     *
     * @param rol
     */
    void save(Rol rol);
}
