package ar.com.product.crud.app.security.impl;

import ar.com.product.crud.app.security.entity.PrimaryUser;
import ar.com.product.crud.app.security.entity.User;
import ar.com.product.crud.app.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    UserService userService;

    @Override
    /**
     * obtiene un usuario por el nombre y si no lo encuentra lanza una exception
     */
    public UserDetails loadUserByUsername(String nameUser) throws UsernameNotFoundException {
        User user = userService.getByUserName(nameUser).get();
        return PrimaryUser.build(user);
    }
}
