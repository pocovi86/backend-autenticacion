package ar.com.product.crud.app.security.jwt;


import ar.com.product.crud.app.security.entity.PrimaryUser;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 *  esta clase genera el token
 * tiene un metodo de validacion para ver que este bien formado y que no este expirado
 *
 */

@Component
public class JwtProvider {

    private final static Logger log = LoggerFactory.getLogger(JwtProvider.class);

    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private int expiration;

    // genera el token
    public String generateToken(Authentication authentication) {
        PrimaryUser primaryUser = (PrimaryUser) authentication.getPrincipal();
        return Jwts.builder().setSubject(primaryUser.getUsername()) /*pasamos el nombre del primaryUser*/
                .setIssuedAt(new Date()) /* fecha de creacion del token*/
                .setExpiration(new Date(new Date().getTime() + expiration * 1000)) /* agregamos la el tiempo de expiracion que seria desde este momento hasta el tiempo que le hemos dado y lo multiplico por mil */
                .signWith(SignatureAlgorithm.HS512, secret).compact(); /* le decimos que tipo de algoritmo es y lo firmamos secret */
    }

    // obtener el nombre usuario del token
    public String getUserNameFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(secret) /* firmamos */
                .parseClaimsJws(token) /* pasamos el token */
                .getBody().getSubject(); /* obtenemos el body y el subject el nombre de usuario */
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser()
                    .setSigningKey(secret) /* pasamos la firma */
                    .parseClaimsJws(token); /* pasamos el token */

            return true;

        } catch (MalformedJwtException e) {
            log.error("Token mal formado");
        } catch (UnsupportedJwtException e) {
            log.error("Token no soportado");
        } catch (ExpiredJwtException e) {
            log.error("Token expirado");
        } catch (IllegalArgumentException e) {
            log.error("Token vacio");
        } catch (SignatureException e) {
            log.error("Fail en la firma");
        }
        return false;
    }
}
