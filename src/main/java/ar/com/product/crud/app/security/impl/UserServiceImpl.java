package ar.com.product.crud.app.security.impl;

import ar.com.product.crud.app.impl.ProductServiceImpl;
import ar.com.product.crud.app.security.entity.User;
import ar.com.product.crud.app.security.repository.UserRepository;
import ar.com.product.crud.app.security.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository repository;

    @Override
    public Optional<User> getByUserName(String nameUser) {
        log.debug("Solicitud para obtener un User por el nombre: {}", nameUser);
        return repository.findByNameUser(nameUser);
    }

    @Override
    public boolean existsByNameUser(String nameUser) {
        log.debug("Solicitud para verificar un User por el nombre: {}", nameUser);
        return repository.existsByNameUser(nameUser);
    }

    @Override
    public boolean existsByEmail(String email) {
        log.debug("Solicitud para verificar un User por el email: {}", email);
        return repository.existsByEmail(email);
    }

    @Override
    public void save(User user) {
        log.debug("Solicitud para crear un User: {}", user);
        repository.save(user);
    }
}
