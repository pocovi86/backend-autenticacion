package ar.com.product.crud.app.repository;

import ar.com.product.crud.app.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    /**
     * busca un producto por medio del nombre y devuelve ese producto
     * @param name
     * @return {@link Optional} of {@link Product}
     */
    Optional<Product> findByName(String name);

    /**
     * verifica si existe o no el prodcuto y devuelve un booleano
     * @param name
     * @return true or false
     */
    boolean existsByName(String name);
}
