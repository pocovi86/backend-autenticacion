package ar.com.product.crud.app.security.enums;

public enum  RolName {
    ROLE_ADMIN, ROLE_USER
}
