package ar.com.product.crud.app.security.service;

import ar.com.product.crud.app.security.entity.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


public interface UserService {

    /**
     * obtiene un User por medio del nombre
     *
     * @param nameUser
     * @return {@link Optional} of {@link User}
     */
    Optional<User> getByUserName(String nameUser);

    /**
     * verifica si existe el User por el nombre
     *
     * @param nameUser
     * @return true or false
     */
    boolean existsByNameUser(String nameUser);

    /**
     * verifica si existe un User por el email
     *
     * @param email
     * @return true or false
     */
    boolean existsByEmail(String email);

    /**
     * guarda un suario en bd
     *
     * @param user
     */
    void save(User user);
}
