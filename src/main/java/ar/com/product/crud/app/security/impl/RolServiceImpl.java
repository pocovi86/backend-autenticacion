package ar.com.product.crud.app.security.impl;

import ar.com.product.crud.app.security.entity.Rol;
import ar.com.product.crud.app.security.enums.RolName;
import ar.com.product.crud.app.security.repository.RolRepository;
import ar.com.product.crud.app.security.service.RolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RolServiceImpl implements RolService {

    private final Logger log = LoggerFactory.getLogger(RolServiceImpl.class);

    @Autowired
    RolRepository repository;

    @Override
    public Optional<Rol> getByRolName(RolName rolName) {
        log.debug("Solicitud para obtener un rol por el nombre: {}", rolName);
        return repository.findByRolName(rolName);
    }

    @Override
    public void save(Rol rol) {
        log.debug("Solicitud para crear un Rol: {}", rol);
        repository.save(rol);
    }
}
