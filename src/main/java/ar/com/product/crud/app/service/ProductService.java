package ar.com.product.crud.app.service;

import ar.com.product.crud.app.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    /**
     * devuelve un listado de productos
     * @return {@link List} of {@link Product}
     */
    List<Product> list();

    /**
     * Obtiene un product por medio de un id
     * @param id
     * @return {@link Optional} of {@link Product}
     */
    Optional<Product> getOne(int id);

    /**
     * obtiene un producto por medio del nombre
     * @param name
     * @return {@link Optional} of {@link Product}
     */
    Optional<Product> getByName(String name);

    /**
     * busca si existe un producto por medio del id
     * @param id
     * @return true or false
     */
    boolean existById(int id);

    /**
     * busca si existe un producto por medio del nombre
     * @param name
     * @return true or false
     */
    boolean existByName(String name);

    /**
     * guarda un product en la bd
     * @param product
     */
    void save(Product product);

    /**
     * eleimina un producto por medio del id
     * @param id
     */
    void delete(int id);
}
