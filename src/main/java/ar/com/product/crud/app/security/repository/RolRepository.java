package ar.com.product.crud.app.security.repository;

import ar.com.product.crud.app.security.entity.Rol;
import ar.com.product.crud.app.security.enums.RolName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolRepository extends JpaRepository<Rol, Integer> {

    /**
     * obtiene un rol a partir del rol de usuario
     *
     * @param rolName
     * @return {@link Optional} of {@link Rol}
     */
    Optional<Rol> findByRolName(RolName rolName);
}
