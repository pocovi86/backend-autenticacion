package ar.com.product.crud.app.security.repository;

import ar.com.product.crud.app.security.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    /**
     * obtiene un usuario a partir del nombre del usuario
     *
     * @param nameUser
     * @return {@link Optional} of {@link User}
     */
    Optional<User> findByNameUser(String nameUser);

    /**
     * verifica en bd si existe el usuario, buscando por el nombre
     *
     * @param nameUser
     * @return true or false
     */
    boolean existsByNameUser(String nameUser);

    /**
     * verifica en bd si existe el usuario, buscando por el email
     *
     * @param email
     * @return true or false
     */
    boolean existsByEmail(String email);
}
