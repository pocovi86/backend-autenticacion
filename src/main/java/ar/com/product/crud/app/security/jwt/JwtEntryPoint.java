package ar.com.product.crud.app.security.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * esta clase comprueba si hay un token valido
 * si no lo hay devuelve una respuesta 401 no autorizado
 */
@Component
public class JwtEntryPoint implements AuthenticationEntryPoint {

    private final static Logger log = LoggerFactory.getLogger(JwtEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        log.error("fail in the method commerce");
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "not authorized");
    }
}
